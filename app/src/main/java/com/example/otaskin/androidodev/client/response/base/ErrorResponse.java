package com.example.otaskin.androidodev.client.response.base;

/**
 * Created by otaskin on 07/12/2016.
 */

public class ErrorResponse {

    private String key;
    private String message;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
