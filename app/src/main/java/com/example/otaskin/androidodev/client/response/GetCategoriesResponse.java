package com.example.otaskin.androidodev.client.response;

import com.example.otaskin.androidodev.client.response.base.BaseResponse;
import com.example.otaskin.androidodev.client.response.dto.CategoryDto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetCategoriesResponse extends BaseResponse implements Serializable {

    private static final long serialVersionUID = -1109016945901679496L;

    private List<CategoryDto> categories = new ArrayList<>();

    public List<CategoryDto> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryDto> categories) {
        this.categories = categories;
    }
}
