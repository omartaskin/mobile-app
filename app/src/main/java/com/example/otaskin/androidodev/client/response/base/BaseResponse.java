package com.example.otaskin.androidodev.client.response.base;

public class BaseResponse {
    private boolean succeed = false;
    private ErrorResponse error;

    public boolean isSucceed() {
        return succeed;
    }

    public void setSucceed(boolean succeed) {
        this.succeed = succeed;
    }

    public ErrorResponse getError() {
        return error;
    }

    public void setError(ErrorResponse error) {
        this.error = error;
    }
}
