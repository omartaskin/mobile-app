package com.example.otaskin.androidodev;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.otaskin.androidodev.client.AdvertServiceClient;
import com.example.otaskin.androidodev.client.CategoryServiceClient;
import com.example.otaskin.androidodev.client.request.AdvertSaveRequest;
import com.example.otaskin.androidodev.client.response.GetCategoriesResponse;
import com.example.otaskin.androidodev.client.response.base.BaseResponse;
import com.example.otaskin.androidodev.client.response.dto.CategoryDto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity {

    private Button button;
    private EditText price;
    private EditText title;
    private EditText description;
    private Spinner category;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        initializeWidgets();


        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, getCategories());
        category.setAdapter(adapter);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    validateFormData();
                    BaseResponse save = save();
                    if (save.isSucceed()) {
                        showAlert(":]", "İlan kaydedildi");
                        clear();
                        return;
                    }

                    if (save.getError() == null) {
                        showAlert("HATA", "Bilinmeyen bir hata oluştu!");
                        return;
                    }

                    String message = createErrorMessage(save);
                    showAlert("HATA", message);
                } catch (Exception e) {
                    showAlert("HATA", "Girdiğiniz değerleri kontrol ediniz!");
                }
            }

            private void validateFormData() throws Exception {
                if (price.getText().toString().isEmpty()) {
                    throw new Exception("price is empty");
                }

                if (title.getText().toString().isEmpty()) {
                    throw new Exception("title is empty");
                }

                if (description.getText().toString().isEmpty()) {
                    throw new Exception("description is empty");
                }

                if (category.getSelectedItem().toString().isEmpty()) {
                    throw new Exception("description is empty");
                }
            }

            private BaseResponse save() {
                AdvertSaveRequest request = createRequest();
                return AdvertServiceClient.getInstance().saveAdvert(request);
            }

            private AdvertSaveRequest createRequest() {
                AdvertSaveRequest request = new AdvertSaveRequest();

                request.setTitle(title.getText().toString());
                request.setPrice(new BigDecimal(price.getText().toString()));
                request.setDescription(description.getText().toString());
                request.setCategory(category.getSelectedItem().toString());

                return request;
            }

            private String createErrorMessage(BaseResponse save) {
                return String.format("Bir hata oluştu: key=%s message=%s", save.getError().getKey(), save.getError().getMessage());
            }

            private void showAlert(String title, String desc) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(title)
                        .setMessage(desc)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    private void initializeWidgets() {
        category = (Spinner) findViewById(R.id.category);
        button = (Button) findViewById(R.id.saveButton);
        price = (EditText) findViewById(R.id.price);
        description = (EditText) findViewById(R.id.description);
        title = (EditText) findViewById(R.id.title);
    }

    private List<String> getCategories() {
        List<String> categoryNames = new ArrayList<>();

        GetCategoriesResponse response = CategoryServiceClient.getInstance().getCategories();
        for (CategoryDto categoryDto : response.getCategories()) {
            categoryNames.add(categoryDto.getName());
        }

        return categoryNames;
    }

    private void clear() {
        final String EMPTY_STING = "";

        title.setText(EMPTY_STING);
        price.setText(EMPTY_STING);
        description.setText(EMPTY_STING);
    }
}
