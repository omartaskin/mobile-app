package com.example.otaskin.androidodev.client;

import org.springframework.web.client.RestTemplate;

public abstract class AbstractServiceClient {

    protected final String BASE_URI = "http://172.27.154.69:8080/";

    protected RestTemplate createClient() {
        return new RestTemplate();
    }
}
