package com.example.otaskin.androidodev.client;

import com.example.otaskin.androidodev.client.request.AdvertSaveRequest;
import com.example.otaskin.androidodev.client.response.base.BaseResponse;

public class AdvertServiceClient extends AbstractServiceClient {

    private final String ADVERT_SAVE_URI = BASE_URI.concat("/advert");

    private static AdvertServiceClient advertServiceClient = new AdvertServiceClient();

    private AdvertServiceClient() {
    }

    public static AdvertServiceClient getInstance() {
        return advertServiceClient;
    }

    public BaseResponse saveAdvert(AdvertSaveRequest request) {
        return createClient().postForEntity(ADVERT_SAVE_URI, request, BaseResponse.class).getBody();
    }
}
