package com.example.otaskin.androidodev.client;

import com.example.otaskin.androidodev.client.response.GetCategoriesResponse;

public class CategoryServiceClient extends AbstractServiceClient {

    private final String GET_ALL_CATEGORIES_URI = BASE_URI + "category";
    private static CategoryServiceClient categoryServiceClient = new CategoryServiceClient();

    private CategoryServiceClient() {
    }

    public static CategoryServiceClient getInstance() {
        return categoryServiceClient;
    }

    public GetCategoriesResponse getCategories() {
        return createClient().getForEntity(GET_ALL_CATEGORIES_URI, GetCategoriesResponse.class).getBody();
    }

}
